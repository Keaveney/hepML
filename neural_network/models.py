# ---- Basic import libraries
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division
import math

# ---- Import Keras deep neural network library
from keras.models import Sequential
from keras.models import Model
from keras.regularizers import l1, l2
from keras.optimizers import Adam, Adadelta
from keras.layers.normalization import BatchNormalization
from keras.layers import Dense, Dropout, Activation, Flatten, Convolution2D, MaxPooling2D, merge
from keras.layers.merge import Concatenate
from keras.layers.core import Lambda
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import multi_gpu_model

# ---- Base model
class Model(object):

    # Model base constructor
    def __init__(self):
        self.estimator = None

    # Model functor
    def __call__(self):
        pass

# ---- Neural network model
class DeepModel(Model):
    '''
    Deep model
    '''
    # Deep model constructor intialize model architecture
    def __init__(self, nlayers=1, nneurons=12, dropout_rate=0.0, l2_norm=0.001, learning_rate=1e-3,
                 activation='relu', kernel_initializer='lecun_normal', optimizer='adam',
                 input_dim=None, input_shape=None, metric='accuracy', loss='binary_crossentropy',
                 save_as='model.h5', report_summary=True, gpu=False, network_architecture="cnn"):
        '''
        Constructor
        '''
        self.nlayers=nlayers
        self.nneurons=nneurons
        self.dropout_rate=dropout_rate
        self.l2_norm=l2_norm
        self.learning_rate=learning_rate
        self.activation=activation
        self.kernel_initializer=kernel_initializer
        self.optimizer=optimizer
        self.input_dim=input_dim
        self.input_shape=input_shape
        self.metric=metric
        self.loss=loss
        self.save_as=save_as
        self.report_summary=report_summary
        self.gpu = gpu
        self.network_architecture = network_architecture
        self.model = None

    # Function to create model architecture
    def __call__(self, **kwargs):
        '''
        Functor
        '''
        if "nlayers" in kwargs:
            self.nlayers = int(kwargs["nlayers"])
        if "nneurons" in kwargs:
            self.nneurons = int(kwargs["nneurons"])
        if "dropout_rate" in kwargs:
            self.dropout_rate=kwargs["dropout_rate"]
        if "l2_norm" in kwargs:
            self.l2_norm=kwargs["l2_norm"]
        if "learning_rate" in kwargs:
            self.learning_rate=kwargs["learning_rate"]
        if "activation" in kwargs:
            self.learning_rate=kwargs["activation"]
        if "kernel_initializer" in kwargs:
            self.kernel_initializer=kwargs["kernel_initializer"]
        if "optimizer" in kwargs:
            self.optimizer=kwargs["optimizer"]
        if "input_dim" in kwargs:
            self.input_dim=int(kwargs["input_dim"])
        if "metric" in kwargs:
            self.metric=kwargs["metric"]
        if "loss" in kwargs:
            self.loss=kwargs["loss"]
        if "save_as" in kwargs:
            self.save_as=kwargs["save_as"]
        if "report_summary" in kwargs:
            self.report_summary=kwargs["report_summary"]
        if "network_architecture" in kwargs:
            self.network_architecture=kwargs["network_architecture"]
        
        return self

    # Function to create model: [m input] -> [n neurons] -> [1 output]
    def create_model(self):
        '''
        Create model
        '''
        print('Build model...')
        return self.build_dnn_fn(input_dim=self.input_dim, nlayers=self.nlayers,
                                 nneurons=self.nneurons, dropout_rate=self.dropout_rate,
                                 l2_norm=self.l2_norm, learning_rate=self.learning_rate,
                                 activation=self.activation, kernel_initializer=self.kernel_initializer,
                                 optimizer=self.optimizer, metric=self.metric, loss=self.loss,
                                 save_as=self.save_as, report_summary=self.report_summary, gpu=self.gpu)

    # Create function returning a compiled network
    def build_dnn_fn(self,
                     nlayers=5, nneurons=50, dropout_rate=0.0, l2_norm=0.001, learning_rate=1e-3,
                     activation='relu', kernel_initializer='lecun_normal', optimizer='adam',
                     input_dim=12, metric='accuracy', loss='binary_crossentropy',
                     save_as='model_dnn.h5', report_summary=True, multiple_gpu=False):
        '''
        build_fn
        '''
        print('Build model...')

        # prevent from giving float value
        self.nlayers   = int(nlayers)
        self.nneurons  = int(nneurons)
        self.dropout_rate = dropout_rate
        self.l2_norm = l2_norm
        self.learning_rate = learning_rate
        self.activation = activation
        self.kernel_initializer = kernel_initializer
        self.optimizer = Adam(lr=self.learning_rate) if optimizer == None else optimizer
        # Setting up the optimization of our weights
        #self.optimizer = = SGD(lr=0.01, decay=1e-6, momentum=0.5, nesterov=True) if optimizer == None else optimizer
        #self.input_dim = input_dim
        self.metric = metric
        self.loss = loss
        self.save_as = save_as
        self.report_summary = report_summary
        self.multiple_gpu = multiple_gpu

        # Start neural network
        self.model = Sequential()

        # Add fully connected layer with an activation function (input layer)
        self.model.add(Dense(units=self.nneurons,
                             input_dim=self.input_dim,
                             kernel_initializer=self.kernel_initializer,
                             activation=self.activation,
                             kernel_regularizer=l2(self.l2_norm)))
        self.model.add(Dropout(self.dropout_rate))

        # Indicate the number of hidden layers
        for layer in range(self.nlayers-1):
            self.model.add(Dense(units=self.nneurons, #int(self.nneurons*1.0/math.pow(2, layer))
                                 kernel_initializer=self.kernel_initializer,
                                 activation=self.activation,
                                 kernel_regularizer=l2(self.l2_norm)))
            # Add dropout layer 
            self.model.add(Dropout(self.dropout_rate))

        # Add fully connected output layer with a sigmoid activation function
        self.model.add(Dense(1,
                             kernel_initializer=self.kernel_initializer,
                             activation='sigmoid',
                             kernel_regularizer=l2(self.l2_norm)))

        # Leverage GPU processing when applicable
        if multiple_gpu:
            self.model = multi_gpu_model(self.model, gpus=4)

        # Compile neural network (set loss and optimize)
        self.model.compile(loss='binary_crossentropy',
                           optimizer=self.optimizer,
                           metrics=[self.metric])

        # Print summary report
        if self.report_summary:
            self.model.summary()

        # Store model to file
        self.model.save(self.save_as)

        # Return compiled network
        return self.model

    def build_cnn_fn(self, input_shape=(1, 8, 8), #(1,28,28) or (8, 8, 2) 
                     kernel_size=(2,2), pool_size=(1, 1), #pool_size=(2, 2)
                     dropout_rate=0.1, activation='relu', 
                     optimizer=Adadelta(), metric='accuracy', 
                     loss='binary_crossentropy', report_summary=True,
                     save_as='model_cnn.h5', multiple_gpu=False): 
        '''build_cnn_fn: creates convolutional neural network architecture.'''
        print('Build model...')

        # Start convolutional neural network 
        self.cnn_model = Sequential()
        self.cnn_model.add(Dropout(0.3,input_shape=self.input_shape))


        # Add a Convolutional layers are comprised of filters and feature maps
        # - The filters are essentially the neurons of the layer. 
        # They have both weighted inputs and generate an output value like a neuron. 
        # The input size is a fixed square called a patch or a receptive field. 
        # If the convolutional layer is an input layer, then the input patch will be pixel values. 
        # If they are deeper in the network architecture, then the convolutional layer will take 
        # input from a feature map from the previous layer.
        # - The feature map is the output of one filter applied to the previous layer. 
        # A given filter is drawn across the entire previous layer, moved one pixel at a time. 
        # Each position results in an activation of the neuron and the output is collected in the feature map.
        self.cnn_model.add(Convolution2D(32, #128, 
                                         kernel_size=(2,2),
                                         padding='same',
                                         activation=activation,
                                         input_shape=self.input_shape#,
                                         #name='conv1_1'
                                     ))
        #model.add(ZeroPadding2D((1, 1)))
        #self.cnn_model.add(MaxPooling2D(pool_size=pool_size, strides=(2, 2)))
        #self.cnn_model.add(Convolution2D(32,
        #                                 kernel_size=(2, 2),
        #                                 activation=activation#,
                                         #name='conv1_2'
                                         #                             ))
        # The pooling layers down-sample the previous layers feature map.
        # - Pooling layers follow a sequence of one or more convolutional layers 
        # and are intended to consolidate the features learned and expressed in the previous layers feature map.
        #self.cnn_model.add(MaxPooling2D(pool_size=pool_size))
        #self.cnn_model.add(BatchNormalization(axis=1, momentum=0.99, center=True, scale=True)) # axis=1 when using convolutional layers

        # Dropout: CNNs have a habit of overfitting, even with pooling layers.
        # - Dropout should be used such as between fully connected layers and perhaps after pooling layers.
        self.cnn_model.add(Dropout(0.3))
        print('dropout rate = ' + str(dropout_rate))

        self.cnn_model.add(Flatten())
        self.cnn_model.add(Dense(32, activation='sigmoid')) # why 16 instead of say 128?

        #  Dropout: CNNs have a habit of overfitting, even with pooling layers. 
        # - Dropout should be used such as between fully connected layers and perhaps after pooling layers.
        self.cnn_model.add(Dropout(dropout_rate))        # saw example where the dropout increased throughout the hidden layers
        #self.cnn_model.add(BatchNormalization(axis=1, momentum=0.99, center=True, scale=True)) # axis=1 when using convolutional layers

        # Add fully connected output layer with a softmax (or sigmoid) activation function
        self.cnn_model.add(Dense(1, activation='sigmoid'))
        #self.cnn_model.add(Dense(2, activation='softmax'))

        # setting up the optimization of our weights 
        #sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)

        # Note: Compile neural network (set loss and optimize)
        # There are two ways to run a single model on multiple GPUs: 
        #     - data parallelism and device parallelism.
        #     - Data parallelism consists in replicating the target model once on each device, 
        #       and using each replica to process a different fraction of the input data. 
        #     - In most cases, what you need is most likely data parallelism.

        # Replicates `cnn_model` on 8 GPUs.
        # This assumes that your machine has 8 available GPUs.
        if multiple_gpu:
            self.cnn_model = multi_gpu_model(self.cnn_model, gpus=4)

        # Compile neural network (set loss and optimize) 
        self.cnn_model.compile(loss=loss,
                               optimizer=optimizer,
                               metrics=[metric])

        # Print summary report 
        if report_summary:
            self.cnn_model.summary()

        # Store model to file
        self.cnn_model.save(save_as)

        # Return compiled network  
        return self.cnn_model

    # Get model
    def estimator(self):
        '''
        Estimator
        '''
        return self.model

    # Define get parameters
    def get_params(self):
        '''
        Get parameters
        '''
        return {'input_dim': self.input_dim, 'nlayers': self.nlayers, 'nneuros': self.nneurons,
                'droupout': self.dropout_rate, 'l2_norm': self.l2_norm, 'learning_rate': self.learning_rate,
                'activation': self.activation, 'kernel_initializer': self.kernel_initializer, 
                'optimizer': self.optimizer, 'metric': self.metric, 'loss': self.loss, 
                'gpu': self.gpu, 'report_summary': self.report_summary}

    # Helper function to simplify the construction of Conv2D followed by a activation function (e.g. ReLu) 
    # followed by a BatchNormalization
    def Conv2DReluBatchNorm(self, n_filter, w_filter, h_filter, inputs):
        activation='relu'
        return BatchNormalization()(Activation(activation=activation)(Convolution2D(n_filter, 
                                                                                    w_filter, 
                                                                                    h_filter, 
                                                                                    border_mode='same')(inputs)))

