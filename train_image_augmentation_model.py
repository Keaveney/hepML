## Create a Convolutional Neural Network with Keras
# Source: https://machinelearningmastery.com/image-augmentation-deep-learning-keras/
from __future__ import print_function

# ---- Import basic libraries
import matplotlib.pyplot as plt
import numpy as np
from time import time

# ---- Import Keras library
import keras
from keras.models import model_from_json
from keras.utils import plot_model
from keras.utils import multi_gpu_model
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint

# Standardize images across the dataset, mean=0, stdev=1
from keras.preprocessing.image import ImageDataGenerator

# ---- Import Scikit-learn library
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc, roc_auc_score

# ---- Import data loader
from neural_network.dataloaders import DataLoader

# ---- Import neural network modelling
from neural_network.models import DeepModel

# ---- Import plotter
from visualization.plotter import Plotter

import multiprocessing

# ---- Declare sample and features to use
signal_sample     = 'data/signal.root'
background_sample = 'data/background.root'
treename          = 'event_mvaVariables_step7_cate4'

features          = [
    'jet_1_px', 'jet_1_py', 'jet_1_pz', #'jet_1_pe', 
    'jet_2_px', 'jet_2_py', 'jet_2_pz', #'jet_2_pe', 
    'jet_3_px', 'jet_3_py', 'jet_3_pz', #'jet_3_pe', 
    'jet_4_px', 'jet_4_py', 'jet_4_pz', #'jet_4_pe', 
#    'jet_5_px', 'jet_5_py', 'jet_5_pz', #'jet_5_pe', 
#    'jet_6_px', 'jet_6_py', 'jet_6_pz', #'jet_6_pe'
    'lepton_1_px', 'lepton_1_py', 'lepton_1_pz',
    'lepton_2_px', 'lepton_2_py', 'lepton_2_pz',
#    'selJet.Pt()', 'selJet.Eta()', 'selJet.Phi()',
    ]

# ---- Dimensions of our images (use 8 x 8 pixel image)
channel = 1
img_depth, img_width, img_height = channel, 8, 8

# ---- Image input data is expressed as a 3-dime matrix of channels x width x height
input_shape = (img_depth, img_width, img_height)
verbose = 2

# ---- Load the analysis dataset 
dataloader =  DataLoader(signal_sample, background_sample, treename, features)
print("Total number of events: {}\nNumber of features (incl. class label): {}\n".format(dataloader.data.shape[0],
                                                                                        dataloader.data.shape[1]))
# ---- Scikit-learn style dataset format from pandas dataframe (example here is sampling from dataset)
sampled_data = dataloader.data #.sample(frac=1e-2, replace=False, random_state=42)
X = sampled_data.drop(['y'], axis=1, inplace=False)
y = sampled_data['y']

# ---- A standard split of the dataset is used to evaluate and compare models
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=42)

# ---- Create two-dim image training & test dataset
start = time()
X_train_jet_img = dataloader.create_image(X_train, 'jet', input_shape)
print("Took %.2f seconds to transform training data into image data format."  % (time() - start))
X_test_jet_img  = dataloader.create_image(X_test,  'jet', input_shape)

start = time()
X_train_lepton_img = dataloader.create_image(X_train, 'lepton', input_shape)
print("Took %.2f seconds to transform training data into image data format."  % (time() - start))
X_test_lepton_img  = dataloader.create_image(X_test, 'lepton', input_shape)

n_process=4
pool = multiprocessing.Pool(processes=n_process)

# ---- Configuration for image datapreparation and augmentation (just-in-time)
# - Sample-wise standardization.
# - Feature-wise standardization.
# - ZCA whitening.
# - Random rotation, shifts, shear and flips. 
# - Dimension reordering.

# compute quantities required for featurewise normalization
# (std, mean, and principal components if ZCA whitening is applied)
# define preperation of data augmentation strategies implemented (translation, rotation, flipping, etc)
# Generate batches of tensor image data with real-time data augmentation. The data will be looped over (in batches) indefinitely.
start = time()
datagen = ImageDataGenerator(featurewise_center=True,            # set input mean to 0 over the dataset (normalization)
                             samplewise_center=False,            # set each sample mean to 0
                             featurewise_std_normalization=True, # divide inputs by std of the dataset
                             samplewise_std_normalization=False, # divide each input by its std
                             #zca_whitening=True,                #  reduces the redundancy in the matrix of pixel images
                             data_format="channels_first",       # images should have shape (samples, channels, height, width)
                             #pool=pool
                             )

# Compute the internal data stats related to the data-dependent transformations, based on an array of sample data. Only required if featurewise_center or featurewise_std_normalization or zca_whitening.
# fits the model parameters from data on batches with real-time data augmentation:  
datagen.fit(X_train_jet_img)

batch_size=256
nb_epoch =80

# Takes numpy data & label arrays, and generates batches of augmented/normalized data. Yields batches indefinitely, in an infinite loop.
X_train_aug = datagen.flow(X_train_jet_img, y_train,
                           batch_size=batch_size,
                           save_to_dir=None, #'images'
                           #save_prefix='aug',
                           #save_format='png'
                           seed=42)
print('{} process, duration: {}'.format(4, time() - start))
pool.terminate()

X_test_aug = datagen.flow(X_test_jet_img, y_test, batch_size=32, seed=42) # NEEDED?? Don't figure (perhaps for the validation can be used)

## Convolutional Neural Network Modeling (CNN)

# ---- Define CNN network architecture modeling
cnn       = DeepModel(input_shape=input_shape)
cnn_model = cnn.build_cnn_fn()

# ---- Use early stopping on training when the validation loss isn't decreasing anymore
early_stopping = EarlyStopping(monitor='val_loss', patience=2)

# ---- Set up callbacks
checkpoint = ModelCheckpoint(
    filepath='cnn_example.h5',
    save_best_only=True
)

# ---- Fit model on training data (use 10% of data for model validation)  
start = time()
cnn_model_history = cnn_model.fit_generator(X_train_aug,
                                            samples_per_epoch=len(X_train_jet_img), 
                                            epochs=nb_epoch,
                                            validation_data=X_test_aug,
                                            #nb_worker=1
                                            verbose=2
                                            )
print("Model training took %.2f seconds." % (time() - start))

# ---- Store model architecture and weights in one file
cnn_model.save('cnn_image_augmentation_model.h5d')

# ---- Store architecture as JSON file and weights separately
json_string = cnn_model.to_json()
cnn_model.save_weights('cnn_image_augmentation_model.h5d')

# ---- Evaluate model test performance
loss, accuracy = cnn_model.evaluate(X_test_jet_img, y_test, verbose=verbose)
print('\nEvaluate test loss \n%s: %.2f%%'    % (cnn_model.metrics_names[0], loss*100))
print('Evaluete test accuracy \n%s: %.2f%%'  % (cnn_model.metrics_names[0], accuracy*100))

# ---- Evaluate model training performance 
loss, accuracy = cnn_model.evaluate(X_train_jet_img, y_train, verbose=verbose)
print('Evaluate train loss \n%s: %.2f%%'     % (cnn_model.metrics_names[0], loss*100))
print('Evaluete train accuracy \n%s: %.2f%%' % (cnn_model.metrics_names[0], accuracy*100))

# ---- Store network achitecture diagram into png
plot_model(cnn_model, to_file='model_image_augmentation_cnn.png')

# ---- List all data in history
print(cnn_model_history.history.keys())

## Visualize Model Training History

# ---- Summarize history for accuracy
plt.plot(cnn_model_history.history['acc'])
plt.plot(cnn_model_history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

# ---- Summarize history for loss
plt.plot(cnn_model_history.history['loss'])
plt.plot(cnn_model_history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

# ---- Calculate AUC of ROC
predictions = cnn_model.predict(X_test_jet_img) #(X_test_jet_img)
fpr, tpr, _ = roc_curve(y_test, predictions)
roc_auc = auc(fpr, tpr)

# Plot all ROC curves
plt.plot(fpr, tpr, lw=2, label='%s (AUC = %0.3f)'%('CNN', roc_auc))
plt.plot([0, 1], [0, 1], 'k--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title("Receiver operating characteristic curve")
leg = plt.legend(loc="best", frameon=True, fancybox=True, fontsize=8)
leg.get_frame().set_edgecolor('w')
frame = leg.get_frame()
frame.set_facecolor('White')
print('AUC: %f' % roc_auc)
plt.show()
